;; freqreg.scm is part of the ff-ad9833 project
;; ff-ad9833 is an audio project for FlashForth and the AD9833
;; Copyright (C) 2021  Christopher Howard

;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

; --- Tools for pre-calculating frequency register values ---

#| assumes that only the 2 least significant bits of msb register load
data need to be saved, and packages them above the 14 bits from the
lsb register load data |#
(define (freqdata-packed f)
  (let* ((raw (/ (* f (expt 2 28)) 25e6))
	 (rounded (inexact->exact (round raw)))
	 (14lsb (logand #b11111111111111 rounded))
	 (14msb (logand #b11111111111111 (ash rounded -14)))
	 (packed16 (logior 14lsb (ash (logand #b11 14msb) 14))))
    (number->string packed16 16)))

(define (tempered-freq hsteps)
  (* 440 (expt 1.059463094359 hsteps)))

(define three-octaves
  (map tempered-freq (iota 36)))

(define three-octaves-packed
  (map freqdata-packed three-octaves))
