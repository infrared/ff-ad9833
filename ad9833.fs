\ **********************************************************************
\
\ ad9833.fs is part of the ff-ad9833 project
\ ff-ad9833 is an audio project for FlashForth and the AD9833

\ Copyright (C) 2021  Christopher Howard

\ SPDX-License-Identifier: GPL-3.0-or-later

\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.

\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.

\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.

\ The purpose of this file is to provide basic words for setting up
\ communication with the ad9833 chip, and for sending data to the ad9833
\ registers.

\ The code assumes by default that fsync line is PD4 (Arduino DP4) but
\ this can be adjust be altering the fsync-ddr, fsync-port, and
\ fsync-bit constants.
\
\ Public words:
\   FREG0_PF FREG1_PF PHREG0_PF PHREG1_PF B28_BT HLB_BT FSELECT_BT
\   PSELECT_BT RESET_BT SLEEP1_BT SLEEP12_BT OPBITEN_BT DIV2_BT MODE_BT
\   fsync-ddr fsync-port fsync-bit init-spi init-fsync fsync-low
\   fsync-high demo-1000hz
\ **********************************************************************

ad9833
marker ad9833

\ **********************************************************************
\ Register prefixes and bit definition constants for the ad9833.
\ These correspond to their equivalent values in the ad9833 data sheet.
\ The control register has bit prefix 00 so we do not waste memory
\ defining it.
\ **********************************************************************

%0100000000000000 constant FREG0_PF
%1000000000000000 constant FREG1_PF
%1100000000000000 constant PHREG0_PF
%1110000000000000 constant PHREG1_PF

#13 constant B28_BT
#12 constant HLB_BT
#11 constant FSELECT_BT
#10 constant PSELECT_BT
#08 constant RESET_BT
#07 constant SLEEP1_BT
#06 constant SLEEP12_BT
#05 constant OPBITEN_BT
#03 constant DIV2_BT
#01 constant MODE_BT

\ **********************************************************************
\ Parameter constants for the fsync line, in case you need to adjust
\ which pin is the fsync line.
\ **********************************************************************

DDRD constant fsync-ddr
PORTD constant fsync-port
#4 constant fsync-bit

\ **********************************************************************
\ Words for setting up communications with the ad9833
\ **********************************************************************

: init-spi ( -- )
    DD_OUT DD_MOSI lshift
    DD_OUT DD_SCK  lshift or
    DD_OUT DD_SS   lshift or DDR_SPI mset
    1           SPR0 lshift
    0           SPR1 lshift or
    CPHA_SMPLED CPHA lshift or
    CPOL_HIDLE  CPOL lshift or
    MSTR_MSTR   MSTR lshift or
    DORD_MSB    DORD lshift or
    SPE_ENAB    SPE  lshift or
    SPIE_DISAB  SPIE lshift or SPCR c!
;

: init-fsync ( -- )  [ 1 fsync-bit lshift ] literal fsync-ddr mset ;

: fsync-low ( -- ) [ 1 fsync-bit lshift ] literal fsync-port mclr ;

: fsync-high ( -- ) [ 1 fsync-bit lshift ] literal fsync-port mset ;

\ **********************************************************************
\ The demo steps are as follows:
\ 1. initialize spi communication
\ 2. set fsync pin to output
\ 3. bring fsync line low
\ 4. Enter chip reset state & prep to load frequency register
\ 5. set phase register to 0
\ 6. load lower 16 bits into frequency register 0
\ 7. load higher 16 bits into frequency register 0
\ 8. exit reset status (initiates output)
\ 9. wait two seconds and then enable chip reset, to silence output
\ 10. bring fysnc line back up to high
\ **********************************************************************

: demo-1000hz ( -- )
    init-spi
    init-fsync
    fsync-low
    [ 1 B28_BT lshift ] literal [ 1 RESET_BT lshift ] literal or 2tx-spi
    PHREG0_PF 2tx-spi
    [ FREG0_PF $29f1 or ] literal 2tx-spi
    FREG0_PF 2tx-spi
    0 2tx-spi
    #2000 ms [ 1 RESET_BT lshift ] literal 2tx-spi
    fsync-high
;
