\ **********************************************************************
\
\ demo-melodies.fs is part of the ff-ad9833 project
\ ff-ad9833 is an audio project for FlashForth and the AD9833
\ Copyright (C) 2021  Christopher Howard

\ SPDX-License-Identifier: GPL-3.0-or-later

\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.

\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.

\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <https://www.gnu.org/licenses/>.

\ The purpose of this module is to provide some demo melodies to go with
\ the score module.

\ public words:
\   demo-rusleeping demo-furelise
\
\ **********************************************************************


demo-melodies
marker demo-melodies

flash

\ **********************************************************************
\ A score and demo-word for `Frere Jacques', a.k.a. `Brother John'.
\ **********************************************************************

create rusleeping
d_quarter nc o5 == d_quarter nd o5 == d_quarter ne o5 == d_quarter nc o5 ==
d_quarter nc o5 == d_quarter nd o5 == d_quarter ne o5 == d_quarter nc o5 ==
d_quarter ne o5 == d_quarter nf o5 == d_half    ng o5 ==
d_quarter ne o5 == d_quarter nf o5 == d_half    ng o5 ==
d_8th     ng o5 == d_8th     na o6 == d_8th     ng o5 == d_8th     nf o5 ==
d_quarter ne o5 == d_quarter nc o5 ==
d_8th     ng o5 == d_8th     na o6 == d_8th     ng o5 == d_8th     nf o5 ==
d_quarter ne o5 == d_quarter nc o5 ==
d_quarter nc o5 == d_quarter ng o4 == d_half    nc o5 ==
d_quarter nc o5 == d_quarter ng o4 == d_half    nc o5 == end-score

ram

: demo-rusleeping
    init-spi init-fsync #2000 init-durations
    fsync-low PHREG0_PF 2tx-spi fsync-high
    rusleeping play-score ;

\ **********************************************************************
\ A score and demo-word for the first half of Beethoven's Fur Elise.
\ I was necessary to do some violence to Fur Elise in order to make it
\ monophonic and to fit it within three octaves.
\ **********************************************************************

flash

create furelise-p1
d_16th ne  o5 == d_16th nd# o5 == d_16th ne  o5 == d_16th nd# o5 ==
d_16th ne  o5 == d_16th nb  o5 == d_16th nd  o5 == d_16th nc  o5 == 
d_8th  na  o5 == d_16th nr  nr == d_16th nc  o4 == d_16th ne  o4 ==
d_16th na  o5 == d_8th  nb  o5 == d_16th nr  nr == d_16th ne  o4 ==
d_16th ng# o4 == d_16th nb  o5 == d_8th  nc  o5 == d_16th nr  nr ==
d_16th ne  o4 == d_16th ne  o5 == d_16th nd# o5 == d_16th ne  o5 ==
d_16th nd# o5 == d_16th ne  o5 == d_16th nb  o5 == d_16th nd  o5 ==
d_16th nc  o5 == d_8th  na  o5 == d_16th nr  nr == d_16th nc  o4 ==
d_16th ne  o4 == d_16th na  o5 == d_8th  nb  o5 == d_16th nr  nr ==
d_16th ne  o4 == d_16th nc  o5 == d_16th nb  o5 == end-score

create furelise-p2 d_quarter na o5 == end-score

create furelise-p3
d_8th  na o5 == d_16th nr nr == d_16th nb o5 == d_16th nc o5 ==
d_16th nd o5 == end-score

create furelise-p4
d_dt-8th ne  o5 == d_16th ng  o4 == d_16th nf  o5 == d_16th ne  o5 ==
d_dt-8th nd  o5 == d_16th nf  o4 == d_16th ne  o5 == d_16th nd  o5 ==
d_dt-8th nc  o5 == d_16th ne  o4 == d_16th nd  o5 == d_16th nc  o5 ==
d_16th   nb  o5 == d_16th ne  o4 == d_16th ne  o5 == d_16th ne  o4 ==
d_16th   ne  o5 == d_16th ne  o4 == d_16th ne  o5 == d_16th ne  o4 ==
d_16th   ne  o5 == d_16th ne  o5 == d_16th ne  o6 == d_16th nf# o4 ==
d_16th   ng  o4 == d_16th nd# o5 == d_16th ne  o5 == d_16th nf# o4 ==
d_16th   ng  o4 == d_16th nd# o5 == d_16th ne  o5 == d_16th nd# o5 ==
d_16th   ne  o5 == d_16th nd# o5 == d_16th ne  o5 == d_16th nb  o5 ==
d_16th   nd  o5 == d_16th nc  o5 == d_16th na  o5 == d_16th ne  o4 ==
d_16th   na  o4 == d_16th nc  o4 == d_16th ne  o4 == d_16th na  o5 ==
d_16th   nb  o5 == d_16th ne  o4 == d_16th ng# o4 == d_16th ne  o4 ==
d_16th   ng# o4 == d_16th nb  o5 == d_16th nc  o5 == d_16th ne  o4 ==
d_16th   na  o4 == d_16th ne  o4 == d_16th ne  o5 == d_16th nd# o5 ==
d_16th   ne  o5 == d_16th nd# o5 == d_16th ne  o5 == d_16th nb  o5 ==
d_16th   nd  o5 == d_16th nc  o5 == d_16th na  o5 == d_16th ne  o4 ==
d_16th   na  o4 == d_16th nc  o4 == d_16th ne  o4 == d_16th na  o5 ==
d_16th   nb  o5 == d_16th ne  o4 == d_16th ng# o4 == d_16th ne  o4 ==
d_16th   nc  o5 == d_16th nb  o5 == end-score

create furelise-p5 d_dt-quarter na o5 == end-score

ram

: demo-furelise
    init-spi init-fsync #4000 init-durations
    fsync-low PHREG0_PF 2tx-spi fsync-high
    furelise-p1 play-score furelise-p2 play-score
    furelise-p1 play-score furelise-p3 play-score
    furelise-p4 play-score furelise-p3 play-score
    furelise-p4 play-score furelise-p5 play-score ;
