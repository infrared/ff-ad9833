# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/ff-ad9833)](https://api.reuse.software/info/codeberg.org/infrared/ff-ad9833)

Readme.md is part of the ff-ad9833 project.

ff-ad9833 is an audio project for FlashForth and the AD9833

Copyright (C) 2021  Christopher Howard

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Description

This project contains FlashForth code for a hobby project to control a
GY-9833-based audio module using an Arduino UNO. These are some blog
posts regarding the project:

https://librehacker.com/tag/ff-ad9833/

I have provided here a system for storing and playing single channel
melodies, which I am calling `scores', using notes from three octaves
of the equal temperament music scale.

I have thoughts also of providing in the future a module for
outputting Audio FSK data communication.

The code should be helpful to those wanting to understand how to
interact with the GY-9833 module, or the AD9833 IC, through
FlashForth. I try to keep the code cleanly-documented and
well-organized.

# Caveats

The GY-9833 is single channel, and by itself it does not have any
capabilities for controlling volume, including attack or release
volume evelopes. So it is less than ideal for creating video game
music or realistic instrument sounds. Nevertheless, it generates a
clean sine wave which is pleasant to hear and which is suitable for
some applications.

The system I have provided for playing scores has imprecise note
duration on the scale of up to a few milliseconds per
note. Consequently it might not keep synchronized tempo with other
machine music generation systems, including another chip running this
same system.

# Code documentation

Each Forth code file has the following documentation in comments:
1. The title of the file
2. A copyright and license header
3. A description of the purpose of the file
4. A list of public words, i.e., words meant to be used outside of the
module itself
5. The code is divided into sections, each containing a section
description.
6. Each dictionary word, not including words like constants and
variables, contains a brief Forth-style stack effect description.

# Requirements

- An Arduino UNO or other board with 328P MC
- MC should be running FlashForth 5
- You will need the `doloop.fs` file, available in the avr/forth
directory of the FlashForth source code. It is not included in the
ff-ad9833 source since it is part of the FlashForth system.
- GY-9833 module driving a speaker. An inexpensive project PCB, which
amplifies output from the GY-9833, can be ordered through a link at the
blog post referenced above. Other ad9833-based modules should work also,
but be aware the code assumes a 25Mhz clock-rate in the
frequency-register calculations
- The code assumes the fsync pin is PD4 (Arduino DP4) but this can be
modified in the ad9833.fs file parameter constants.

# Compiling

After setting up a FlashForth shell connection, run these commands in
the shell.

In FlashForth, you only need to compile (#send) the code once, and
then you can run the compiled words later, even after resetting the
microcontroller. For more information about FlashForth, visit

[FlashForth Web Site](https://flashforth.com/)

## Core words and definitions for controlling the ad9833

    #send doloop
    #send util
    #send 328p
    #send ad9833

## Musical notes and scores of notes

    #send notes
    #send score
    #send demo-melodies

# Running Demos

After setting up a FlashForth shell connection, enter the `words`
command and look for words to execute which are prefixed with `demo-`.
